MailAdmin::Application.routes.draw do
  devise_for :managers, path_names: {
    sign_in: 'login', sign_out: 'logout'
  }
# :path_names => { :sign_in => 'login', :sign_out => 'logout', :password => 'secret', :confirmation => 'verification', :unlock => 'unblock', :registration => 'register', :sign_up => 'cmon_let_me_in' }
  resources :users do
    post :block, on: :member
  end

  resources :domains do
    post :block, on: :member
  end

  resource :profile

  root :to => 'site#index'
end
