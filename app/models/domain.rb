class Domain < ActiveRecord::Base
  default_scope order(:name)

  has_many :users

  validates :name, presence: true, uniqueness: true
  validate :domain_name

  def to_s
    name
  end

  protected

  def domain_name
    begin
      domain = Whois.whois(name)
      if domain.available?
        errors.add(:name, 'domain name not registered')
      end
    rescue Whois::ServerNotFound => e
      errors.add(:name, 'invalid domain name')
    end
  end
end
