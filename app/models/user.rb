class User < ActiveRecord::Base
  before_validation :set_gid_uid
  before_save :hash_password, if: lambda { |a| a.password_changed? }

  default_scope order(:name)

  belongs_to :domain

  validates :uid, :gid, numericality: true
  validates :username, uniqueness: { scope: :domain_id }
  validates :username, :name, :uid, :gid, :domain_id, presence: true
  validates :password, presence: true, length: { minimum: 6 }, on: :create
  validates :password, confirmation: true, length: { minimum: 6 },
    unless: lambda { |a| a.password.blank? }

  attr_accessor :password_confirmation

  protected

  def set_gid_uid
    self.uid = Rails.configuration.mail.uid.to_i
    self.gid = Rails.configuration.mail.gid.to_i
  end

  def hash_password
    salt = SecureRandom.hex
    hash = Base64.encode64(Digest::SHA256.digest(password + salt) + salt)
    self.password = '{SSHA256}%s' % hash
  end
end
