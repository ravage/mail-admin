class DomainsController < ApplicationController
  include Blockable

  respond_to :html
  def index
    @domains = Domain.all
    respond_with(@domains)
  end

  def new
    @domain = Domain.new
    respond_with(@domain)
  end

  def edit
    @domain = Domain.find(params[:id])

    respond_with(@domain)
  end

  def create
    @domain = Domain.new(params[:domain])

    if @domain.save
      flash[:success] = 'Domain successfully saved!'
    else
      flash[:error] = 'There was an error while saving the domain!'
    end

    respond_with(@domain, :location => :domains)
  end

  def update
    @domain = Domain.find(params[:id])

    if @domain.update_attributes(params[:domain])
      flash[:success] = 'Domain successfully saved!'
    else
      flash[:error] = 'There was an error while trying to save the domain!'
    end

    respond_with(@domain, location: :domains)
  end

  def destroy
    @domain = Domain.find(params[:id])

    @domain.destroy

    respond_with(@domain)
  end
end
