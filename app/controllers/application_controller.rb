class ApplicationController < ActionController::Base
  before_filter :authenticate_manager!
  protect_from_forgery
  layout :select_layout

  protected

  def select_layout
    if devise_controller?
      'site'
    else
      'backoffice'
    end
  end

  def after_sign_out_path_for(resource_or_scope)
    new_manager_session_path
  end

  def after_sign_in_path_for(resource)
    stored_location_for(resource) || url_for(:users)
  end
end
