class ProfilesController < ApplicationController
  def show
    @manager = current_manager
  end

  def update
    @manager = current_manager

    result ||= @manager.update_attributes(params[:manager]) if params.key?(:manager)
    result ||= @manager.update_with_password(params[:password]) if params.key?(:password)

    if result
      sign_in(@manager, :bypass => true)
      redirect_to :users, notice: 'Profile successfully updated!'
    else
      render :show
    end
  end
end
