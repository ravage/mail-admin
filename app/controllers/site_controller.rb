class SiteController < ActionController::Base
  layout 'site'

  def index
    redirect_to new_manager_session_path
  end
end
