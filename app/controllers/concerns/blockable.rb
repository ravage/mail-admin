module Blockable
  extend ActiveSupport::Concern

  def block
    resource = controller_name.classify.constantize.find(params[:id])

    resource.toggle!(:active)
    respond_with(resource, location: controller_name.to_sym)
  end
end
