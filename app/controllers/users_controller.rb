class UsersController < ApplicationController
  include Blockable

  respond_to :html

  def index
    @users = User.all

    respond_with(@users)
  end

  def new
    @user = User.new

    respond_with(@user)
  end

  def edit
    @user = User.find(params[:id])

    respond_with(@user)
  end

  def create
    @user = User.new(params[:user])
    
    if @user.save
      flash[:success] = 'User successfully saved!'
    else
      flash[:error] = 'There was an error while saving the users!'
    end

    respond_with(@user, location: :users)
  end

  def update
    @user = User.find(params[:id])

    if @user.update_attributes(params[:user])
      flash[:success] = 'User successfully updated!'
    else
      flash[:error] = 'There was an error while updating the user!'
    end

    respond_with(@user, location: :users)
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_with(@user)
  end
end
