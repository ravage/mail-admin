module ApplicationHelper

  def toggle_action(status)
    (status) ? 'Disable' : 'Enable'
  end

  def status(state)
    result = (state) ? 'Active' : 'Inactive'

    content_tag(:span, result, class: "#{result.downcase} label")
  end
end
