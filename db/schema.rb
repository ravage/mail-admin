# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120124012544) do

  create_table "domains", :force => true do |t|
    t.string   "name",       :limit => 128,                   :null => false
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at"
    t.boolean  "active",                    :default => true, :null => false
  end

  create_table "managers", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",                       :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                                            :null => false
    t.datetime "updated_at",                                            :null => false
  end

  add_index "managers", ["email"], :name => "index_managers_on_email", :unique => true
  add_index "managers", ["reset_password_token"], :name => "index_managers_on_reset_password_token", :unique => true
  add_index "managers", ["unlock_token"], :name => "index_managers_on_unlock_token", :unique => true

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "users", :force => true do |t|
    t.string   "name",       :limit => 128,                   :null => false
    t.string   "username",   :limit => 128,                   :null => false
    t.string   "home",       :limit => 512, :default => "",   :null => false
    t.integer  "uid",                                         :null => false
    t.integer  "gid",                                         :null => false
    t.string   "password",   :limit => 256,                   :null => false
    t.integer  "domain_id",  :limit => 8,                     :null => false
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at"
    t.boolean  "active",                    :default => true, :null => false
  end

end
