CREATE TABLE domains (
  id          BIGSERIAL PRIMARY KEY NOT NULL,
  name        VARCHAR(128) NOT NULL,
  active      BOOLEAN DEFAULT true NOT NULL,
  created_at  TIMESTAMP DEFAULT LOCALTIMESTAMP NOT NULL,
  updated_at  TIMESTAMP
);

CREATE TABLE users (
  id          BIGSERIAL PRIMARY KEY NOT NULL,
  name        VARCHAR(128) NOT NULL,
  username    VARCHAR(128) NOT NULL,
  home        VARCHAR(512) DEFAULT '' NOT NULL,
  "uid"       INT NOT NULL,
  gid         INT NOT NULL,
  password    VARCHAR(256) NOT NULL,
  active      BOOLEAN DEFAULT true NOT NULL,
  domain_id   BIGINT NOT NULL,
  created_at  TIMESTAMP DEFAULT LOCALTIMESTAMP NOT NULL,
  updated_at  TIMESTAMP
);

--
ALTER TABLE users
  ADD CONSTRAINT fk_user_domains FOREIGN KEY (domain_id) REFERENCES domains (id);
